/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author Kevin Tarazona <tkalejadro122@gmail.com>
 */
public class InvertirArray_Recursivo {
    
    public static String decoradorInverso(int[]a){
        
        String resultado="";
        for(int i = 0; i<a.length; i++){
                resultado+=a[i]+",";
            }
        resultado+="\n";
        
        if(a.length==0)
            throw new RuntimeException("No se puede hallar el número mayor de un vector de tamaño 0");
        
        else if(a.length==1)
            resultado=String.valueOf(a[0]);
        
        else{
            invertir(a, 0, a.length/2,a.length);
            for(int i = 0; i<a.length; i++){
                resultado+=a[i]+",";
            }
        }
        return resultado;
    }
    
    public static void invertir(int[]a,int indice, int mitad,int tamanooriginal){
        int c=0;
        if(indice<mitad){
            c=a[tamanooriginal-indice-1];
            a[tamanooriginal-indice-1]=a[indice];
            a[indice]=c;
            invertir(a, indice+1,mitad,tamanooriginal);
        }
    }
    
    public static void main(String[] args) {
            try{
            int[]a={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};
            System.out.println(decoradorInverso(a));
            }
            catch(Exception e){
                System.out.println(e);
            }
    }

}
